import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Omero Francisco Bertol (27/09/2016)
 */

public class Pratica31 {
  private static String meuNome = "omERO franCISCO beRTol";
  private static GregorianCalendar dataNascimento = new GregorianCalendar(1965, Calendar.NOVEMBER, 30);
  private static Date inicio;
  private static Date fim;
    
  // programa principal (main)
  public static void main(String[] args) {
    // início das operações 
    inicio = new Date(System.currentTimeMillis()); // pegando a data atual  
      
    // 5. Exiba (com System.out.println) o seu nome (o conteúdo da variável meuNome) todo em letras maiúsculas.
    System.out.println(meuNome.toUpperCase());
    
    // 6. Extraia, usando os métodos adequados, as iniciais do seu nome, exceto do último e exiba no formato "Bertol, O. F."    
    System.out.println(meuNome.substring(16, 17).toUpperCase() + meuNome.substring(17, 22).toLowerCase() + ", " + 
      meuNome.substring(0, 1).toUpperCase() + ". " + meuNome.substring(6, 7).toUpperCase() + "."); 
    
    // (24 * 60 * 60 * 1000) corresponde a quantidade de milissegundos de um dia
    GregorianCalendar hoje = new GregorianCalendar();
    long diasVividos = (hoje.getTime().getTime() - dataNascimento.getTime().getTime()) / (24 * 60 * 60 * 1000);
    System.out.println(diasVividos);
   
    // fim das operações
    fim = new Date(System.currentTimeMillis());
    
    // 8. Crie a variável inicio do tipo Date antes de iniciar o processamento dos itens 4 a 7 e a variável fim após. 
    //    Exiba o tempo decorrido, em milissegundos, entre o início da execução do item 4 e o final da execucão do item 7.
    System.out.println(fim.getTime() - inicio.getTime());
  }
    
}